package org.nevack.bsuir.financemanager.ui.accounts

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.managers.FileSharingManager
import org.nevack.bsuir.financemanager.views.Chart


class AccountsFragment : Fragment() {
    private val viewModel: AccountsViewModel by viewModel()

    private lateinit var recycler: RecyclerView
    private lateinit var adapter: AccountsAdapter

    private lateinit var chart: Chart

    companion object {
        fun newInstance() = AccountsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_accounts, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_share_report -> {
                val accounts = viewModel.getAccounts().value

                if (accounts != null) {
                    val text = accounts.fold("") { acc, account ->
                        acc + account.name + "\n"
                    }

                    val intent = Intent.createChooser(
                            get<FileSharingManager>().reportSharingIntent(text),
                            getString(R.string.message_share_report_using)
                    )
                    startActivity(intent)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.accounts_fragment, container, false)

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (activity as? AppCompatActivity)?.setSupportActionBar(toolbar)

        recycler = view.findViewById(R.id.rv_accounts)
        recycler.layoutManager =
                LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recycler.setHasFixedSize(true)
        recycler.addOnScrollListener(CardScrollListener(0.25f))

        adapter = AccountsAdapter()
        recycler.adapter = adapter

        chart = view.findViewById(R.id.chart)
        chart.setData(arrayListOf(700.0, 350.0, 650.0, 650.0, 600.0, 500.0, 700.0))

        with (PagerSnapHelper()) { attachToRecyclerView(recycler) }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getAccounts().observe(this) {
            adapter.submitData(it)
        }

        viewModel.operations.observe(this) {
            chart.setData(it.map { o -> -o.value }.take(7).asReversed())
        }
    }
}
