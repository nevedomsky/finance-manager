package org.nevack.bsuir.financemanager.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import org.nevack.bsuir.financemanager.R

class Chart @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : View(context, attrs, defStyle, defStyleRes) {

    private val colors = intArrayOf(context.getColor(R.color.primaryLightColor), 0)

    private val density = context.resources.displayMetrics.density

    private val inset = density * 16

    private val gradientPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }

    private val dottedPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = 1 * density
        pathEffect = DashPathEffect(floatArrayOf(3 * density, 3 * density), 0f)
        color = context.getColor(R.color.colorDivider)
    }

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = context.getColor(R.color.primaryColor)
        strokeWidth = 2 * density
    }

    private val pointPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = context.getColor(R.color.primaryDarkColor)
    }

    private val path = Path()


    private var data = emptyList<Double>()
    private var max = .0
    private var min = .0

    private var chartw = 0
    private var charth = 0
    private var xscale = 0f
    private var yscale = 0f

    fun setData(new: List<Double>) {
        if (new.size < 2) return
        data = new

        invalidate()
    }

    private fun calcPositions() {
        max = data.max() ?: .0
        min = data.min() ?: .0

        xscale = (width - (paddingStart + paddingEnd)) / (data.size - 1).toFloat()
        yscale = (height - 2 * (paddingTop + paddingBottom)) / (max - min).toFloat()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        charth = h - (paddingBottom + paddingTop)
        chartw = w - (paddingStart + paddingEnd)

        val gradient = LinearGradient(
                0f,
                paddingTop.toFloat(),
                0f,
                h - paddingBottom.toFloat(),
                colors,
                null,
                Shader.TileMode.CLAMP
        )

        gradientPaint.shader = gradient
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        calcPositions()

        path.reset()

        path.moveTo(paddingLeft.toFloat() - density / 2, (height).toFloat())
        path.moveTo(paddingLeft.toFloat() - 3 * density / 2, height - 2 * paddingBottom - (data[0] - min).toFloat() * yscale)

        for ((i, point) in data.withIndex()) {
            path.lineTo(
                    paddingStart + i * xscale,
                    height - 2 * paddingBottom - (point - min).toFloat() * yscale
            )
        }

        path.lineTo(paddingStart + data.lastIndex * xscale + 3 * density / 2, height - 2 * paddingBottom - (data.last() - min).toFloat() * yscale)
        path.lineTo(paddingStart + data.lastIndex * xscale + 3 * density / 2, (height).toFloat())
        path.lineTo(paddingStart.toFloat(), (height).toFloat())

        canvas.drawPath(path, gradientPaint)

        for (i in 1 until data.size) {
            canvas.drawLine(
                    paddingStart + (i - 1) * xscale,
                    height - 2 * paddingBottom - (data[i - 1] - min).toFloat() * yscale,
                    paddingStart + i * xscale,
                    height - 2 * paddingBottom - (data[i] - min).toFloat() * yscale,
                    strokePaint
            )
        }

        for (i in 1 until data.lastIndex) {
            canvas.drawLine(
                    paddingStart + i * xscale,
                    paddingTop.toFloat(),
                    paddingStart + i * xscale,
                    (height).toFloat(),
                    dottedPaint
            )
        }

        for ((i, point) in data.withIndex()) {
            canvas.drawCircle(
                    paddingStart + i * xscale,
                    height - 2 * paddingBottom - (point - min).toFloat() * yscale,
                    3 * density,
                    pointPaint
            )
        }
    }
}