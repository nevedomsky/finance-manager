package org.nevack.bsuir.financemanager.ui.accounts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.format
import org.nevack.bsuir.financemanager.model.account.Account

class AccountsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var accounts: List<Account> = emptyList()
    private var sum: Double = 0.0

    fun submitData(data: List<Account>) {
        accounts = data
        sum = accounts.sumByDouble { it.value }
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (position > 0) return R.layout.account_item
        return R.layout.total_item
    }

    inner class TotalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val total: TextView = view.findViewById(R.id.total_value)
        val currency: TextView = view.findViewById(R.id.total_currency)
    }

    inner class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.account_name)
        val value: TextView = view.findViewById(R.id.account_value)
        val currency: TextView = view.findViewById(R.id.account_currency)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            R.layout.total_item -> TotalViewHolder(view)
            R.layout.account_item -> AccountViewHolder(view)
            else -> throw IllegalArgumentException("Put a valid viewType")
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position > 0) {
            if (holder is AccountViewHolder) {
                val account = accounts[position - 1]
                (holder.view as MaterialCardView).setCardBackgroundColor(account.color)
                holder.name.text = account.name
                holder.value.text = account.value.format()
                holder.currency.text = account.currency
            }
        } else {
            if (holder is TotalViewHolder) {
                holder.total.text = sum.format()
                holder.currency.text = PreferenceManager
                        .getDefaultSharedPreferences(holder.itemView.context)
                        .getString("default_currency", "USD")
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = accounts.size + 1
}