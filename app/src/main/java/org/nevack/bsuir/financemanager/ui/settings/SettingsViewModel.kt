package org.nevack.bsuir.financemanager.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import org.nevack.bsuir.financemanager.model.currency.Currency
import org.nevack.bsuir.financemanager.model.currency.CurrencyRepository

class SettingsViewModel(private val repository: CurrencyRepository) : ViewModel() {

    fun getCurrencies(): LiveData<List<Currency>> = repository.getCurrencies()

//    fun getCurrencyNames() = Transformations.map(getCurrencies()) {
//        it.map { currency -> currency.name }
//    }
//
//    fun getCurrencyCodes() = Transformations.map(getCurrencies()) {
//        it.map { currency -> currency.code }
//    }
    fun getCurrency(code: String) = repository.getCurrency(code)
}