package org.nevack.bsuir.financemanager.managers

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import androidx.core.content.getSystemService

class NetworkManager(context: Context) {
    private val manager = context.getSystemService<ConnectivityManager>()!!

    val activeNetwork: NetworkInfo = manager.activeNetworkInfo
    val isConnected: Boolean = activeNetwork.isConnected
    val isWifi: Boolean = isConnected && manager.getNetworkCapabilities(manager.activeNetwork)
            .hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
}