package org.nevack.bsuir.financemanager.ui.accounts

import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import kotlin.math.abs

class CardScrollListener(private val coefficient: Float) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        for (view in recyclerView.children) {
            val x = (recyclerView.width - view.width) / 2

            val c = with(view) {
                coefficient + (1 - coefficient) * abs(1f - ((abs(left - x)).toFloat() / width))
            }

            view.apply {
                alpha = c
                if (this is MaterialCardView) {
                    cardElevation = c * maxCardElevation
                    scaleX = (c - 0.25f) / 7.5f + 0.9f
                    scaleY = (c - 0.25f) / 7.5f + 0.9f
                }
            }
        }
    }
}