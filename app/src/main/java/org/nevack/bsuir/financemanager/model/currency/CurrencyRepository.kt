package org.nevack.bsuir.financemanager.model.currency

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.nevack.bsuir.financemanager.managers.NetworkManager
import org.nevack.bsuir.financemanager.nbrb.NbrbService
import java.util.*
import kotlin.coroutines.CoroutineContext

class CurrencyRepository(
        private val dao: CurrencyDao,
        private val manager: NetworkManager,
        private val service: NbrbService,
        private val preferences: SharedPreferences
) : CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = IO + job

    fun getCurrencies(): LiveData<List<Currency>> {
        val updateInterval = 12 * 60 * 60 * 1000L
        val date = Date()
        val interval = date.time - preferences.getLong("last_updated", 0)
        val itOutdated = interval < 0 || interval > updateInterval

        if (itOutdated && manager.isWifi) {
            val data = MutableLiveData<List<Currency>>()
            launch {
                val rates = service.rates()
                val currencies = service.currencies()

                val map = currencies.await().filter { it.dateEnd.after(date) }
                        .associateBy {it.abbreviation }

                rates.await().map { Currency.fromNbrb(map[it.abbreviation]!!, it) }
                        .plus(Currency.BELARUSSIAN)
                        .run {
                            data.postValue(this)
                            forEach { dao.insert(it) }
                        }
                preferences.edit {
                    putLong("last_updated", date.time)
                }
            }
            return data
        } else return dao.getAllCurrencies()
    }

    fun getCurrency(code: String) = dao.getCurrencyByCode(code)
}