package org.nevack.bsuir.financemanager.model.currency

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CurrencyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currency: Currency)

    @Update
    fun update(vararg currencies: Currency)

    @Delete
    fun delete(vararg currencies: Currency)

    @Query("SELECT * FROM currencies")
    fun getAllCurrencies() : LiveData<List<Currency>>

    @Query("SELECT * FROM currencies WHERE code=:code")
    fun getCurrencyByCode(code: String) : LiveData<List<Currency>>
}