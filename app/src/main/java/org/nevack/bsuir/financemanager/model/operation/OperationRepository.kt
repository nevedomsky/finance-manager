package org.nevack.bsuir.financemanager.model.operation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.nevack.bsuir.financemanager.Event
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.account.AccountDao
import kotlin.coroutines.CoroutineContext

class OperationRepository(
        private val historyDao: HistoryDao,
        private val accountDao: AccountDao
): CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = IO + job

    fun getOperations() = historyDao.getAllHistory()

    fun getOperation(id: Long) = historyDao.getOperationById(id)

    fun getOperationsFromAccount(account: Account) = historyDao.getHistoryFromAccount(account.id)

    private var _addedEvent = MutableLiveData<Event<Unit>>()
    val operationAdded: LiveData<Event<Unit>>
        get() = _addedEvent

    private var _removedEvent = MutableLiveData<Event<Unit>>()
    val operationRemoved: LiveData<Event<Unit>>
        get() = _removedEvent

    fun addOperation(operation: Operation) {
        launch {
            val account = accountDao.getAccountSync(operation.account)

            account.value += operation.value

            accountDao.update(account)

            historyDao.insert(operation)
        }

        _addedEvent.value = Event(Unit)
    }

    fun updateOperation(operation: Operation) {
        launch {
            val oldOperation = async { historyDao.getOperationSync(operation.id) }
            val account = accountDao.getAccountSync(operation.account)

            val delta = operation.value - oldOperation.await().value

            account.value += delta

            accountDao.update(account)
            historyDao.insert(operation)
        }
    }

    fun removeOperation(operation: Operation) {
        launch {
            historyDao.delete(operation)
            val account = accountDao.getAccountSync(operation.account)

            account.value -= operation.value
            accountDao.update(account)
        }

        _removedEvent.value = Event(Unit)
    }
}