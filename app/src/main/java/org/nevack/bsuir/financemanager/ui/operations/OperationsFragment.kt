package org.nevack.bsuir.financemanager.ui.operations

import android.content.res.Configuration
import android.os.Bundle
import android.os.Message
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.operations_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.EventObserver
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.model.operation.Operation
import org.nevack.bsuir.financemanager.startActivity
import org.nevack.bsuir.financemanager.ui.addoperation.AddOperationActivity

class OperationsFragment : Fragment() {

    companion object {
        fun newInstance() = OperationsFragment()
    }

    private val viewModel: OperationsViewModel by viewModel()

    private lateinit var recyclerAdapter: OperationsAdapter
    private lateinit var recycler: RecyclerView
    private lateinit var chips: ChipGroup

    private var count = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.operations_fragment, container, false)

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (activity as? AppCompatActivity)?.setSupportActionBar(toolbar)

        val toolbarLayout = view.findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        toolbarLayout.title = getString(R.string.title_history)

        chips = view.findViewById(R.id.filter_chips)

        chips.addView(createChip("First"))
        chips.addView(createChip("Second"))
        chips.addView(createChip("Third"))

        recycler = view.findViewById(R.id.rv_operations)

        recyclerAdapter = OperationsAdapter(requireContext(), viewModel::removeOperation) {
            viewModel.openOperationDetails(it.id)
        }

        recycler.apply {
            layoutManager = if (isLandscape()) {
                GridLayoutManager(context, 2) }
            else {
                LinearLayoutManager(context)
            }
            setHasFixedSize(true)
            adapter = recyclerAdapter
        }

        recycler.adapter = recyclerAdapter

        view.findViewById<FloatingActionButton>(R.id.fab_add).setOnClickListener {
            activity?.startActivity<AddOperationActivity>()
        }

        setHasOptionsMenu(true)

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.operations_menu, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.operations.observe(this, Observer {
            textview_no_items.isGone = it.isNotEmpty()
            recycler.isVisible = it.isNotEmpty()

            if (it.size == count + 1) {
            }

            progress.isGone = true

            recyclerAdapter.submitList(it)
            count = it.size
        })

        viewModel.navigateToDetails.observe(this, EventObserver {
            activity?.startActivity(AddOperationActivity.newIntent(requireContext(), it))
        })

        viewModel.addedEvent.observe(this, EventObserver {
            showMessage(getString(R.string.snackbar_operation_added))
        })

        viewModel.removedEvent.observe(this, EventObserver {
            showMessage(getString(R.string.snackbar_operation_removed))
        })
    }


    private fun showMessage(
            text: String,
            duration: Int = Snackbar.LENGTH_SHORT
    ) {
        Snackbar.make(
                snackbar_container,
                text,
                duration
        ).show()
    }

    private fun createChip(
            chipText: String,
            closeAction: () -> Unit = {}
    ) : Chip {
        return Chip(context).apply {
            isCloseIconVisible = true
            isCheckable = false
            text = chipText
            setOnCloseIconClickListener {
                closeAction()
                isEnabled = false
                animate().setDuration(250L)
                        .alpha(0f)
                        .scaleY(0f)
                        .scaleX(0f)
                        .translationYBy(height.toFloat() / 2)
                        .withEndAction { chips.removeView(this) }
            }
        }
    }

    private fun isLandscape() : Boolean {
        return Configuration.ORIENTATION_LANDSCAPE== resources.configuration.orientation
    }
}
