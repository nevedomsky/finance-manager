package org.nevack.bsuir.financemanager.ui.addoperation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.*
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.category.Category
import kotlin.math.abs
import kotlin.math.max

class AddOperationFragment : Fragment() {

    companion object {

        private const val ARG_NAME = "item"

        fun newInstance() = AddOperationFragment()

        fun newInstance(operation: Long) : AddOperationFragment {
            val args = Bundle().apply { putLong(ARG_NAME, operation) }
            return AddOperationFragment().apply { arguments = args }
        }
    }

    private val viewModel: AddOperationViewModel by viewModel()

    private lateinit var categories: List<Category>
    private lateinit var accounts: List<Account>

    private lateinit var spinnerAccounts: Spinner
    private lateinit var spinnerCategories: Spinner
    private lateinit var spinnerCurrencies: Spinner
    private lateinit var editName: TextInputEditText
    private lateinit var editNameLayout: TextInputLayout
    private lateinit var editValue: TextInputEditText
    private lateinit var editValueLayout: TextInputLayout
    private lateinit var radioSpent: RadioButton
    private lateinit var radioIncome: RadioButton
    private lateinit var buttonSave: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments
        if (args != null && args.containsKey(ARG_NAME)) {
            viewModel.setOperation(args.getLong(ARG_NAME))
        } else viewModel.setOperation(-1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.add_operation_fragment, container, false)

        spinnerAccounts  = view.findViewById(R.id.spinner_accounts)
        spinnerCategories = view.findViewById(R.id.spinner_categories)
        spinnerCurrencies = view.findViewById(R.id.spinner_test)
        editName = view.findViewById(R.id.edittext_name)
        editNameLayout = view.findViewById(R.id.edit_name_layout)
        editValue = view.findViewById(R.id.edittext_value)
        editValueLayout = view.findViewById(R.id.edit_value_layout)
        radioSpent = view.findViewById(R.id.radio_spent)
        radioIncome = view.findViewById(R.id.radio_income)
        buttonSave = view.findViewById(R.id.button_save)

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (activity as? AppCompatActivity)?.setSupportActionBar(toolbar)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.accounts.observe(this, Observer {
            val adapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_list_item_1,
                    it
            )
            spinnerAccounts.adapter = adapter
            accounts = it
        })

        viewModel.categories.observe(this) {
            val adapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_list_item_1,
                    it
            )
            spinnerCategories.adapter = adapter
            categories = it
        }

        viewModel.currencies.observe(this) {
            val adapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_list_item_1,
                    it.map { c -> c.code }
            )
            spinnerCurrencies.adapter = adapter
        }

        viewModel.operation.observe(this) { operation ->
            if (operation.name.isNotBlank()) editName.setText(operation.name)
            operation.value.let {
                if (it != 0.0) editValue.setText(abs(it).toString())

                if (it > 0) {
                    radioIncome.isChecked = true
                } else {
                    radioSpent.isChecked = true
                }
            }
        }

        spinnerCategories.setOnItemSelectedListener {
            viewModel.selectedCategory = it
        }

        spinnerAccounts.setOnItemSelectedListener {
            viewModel.selectedAccount = it
        }

        editName.addTextChangedListener {
            viewModel.name = it
        }

        editValue.addTextChangedListener{
            if (it.startsWith(".") || it.startsWith(",")) {
                val withZero = "0" + editValue.text.toString()
                editValue.setText(withZero)

                editValue.moveToEnd()
                return@addTextChangedListener
            }

            if (it.startsWith("00")) {
                val firstNonZero = max(it.indexOfFirst { c -> c != '0' }, 1)
                editValue.setText(it.substring(firstNonZero))

                editValue.moveToEnd()
                return@addTextChangedListener
            }

            viewModel.value = it
        }

        radioSpent.setOnCheckedChangeListener { _, isChecked ->
            viewModel.isSpent = isChecked
        }

        buttonSave.setOnClickListener {
            viewModel.addOperationToAccount()
        }

        viewModel.saved.observe(this, EventObserver {
            activity?.finish()
        })

        viewModel.valueError.observe(this, EventObserver { error ->
            editValueLayout.error =
                    if (error) getText(R.string.error_value_invalid) else null
            refreshSave()
        })

        viewModel.nameError.observe(this, EventObserver { error ->
            editNameLayout.error =
                    if (error) getText(R.string.error_cannot_be_blank) else null
            refreshSave()
        })
    }

    private fun refreshSave() {
        val nameError = viewModel.nameError.value?.peek() ?: true
        val valueError = viewModel.valueError.value?.peek() ?: true

        buttonSave.isEnabled = !nameError && !valueError
    }
}
