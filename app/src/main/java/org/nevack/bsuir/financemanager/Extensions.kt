package org.nevack.bsuir.financemanager

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import java.util.*

fun Double.format(digits: Int = 2): String = "%.${digits}f".format(this)

val Resources.locale: Locale
    get() = configuration.locales[0]


inline fun <reified T : Activity> Activity.startActivity(
        initializer: Intent.() -> Unit = {}
) {
    val intent = Intent(this, T::class.java).apply { initializer() }
    startActivity(intent)
}

fun lerp(a: Float, b: Float, f: Float): Float {
    return a + f * (b - a)
}

infix fun Pair<Float, Float>.lerp(f: Float): Float {
    return first + f * (second - first)
}

inline fun EditText.addTextChangedListener(
        crossinline block: (String) -> Unit
) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?,
                                       f: Int, c: Int, a: Int) {}
        override fun onTextChanged(
                text: CharSequence?,
                start: Int, before: Int, count: Int
        ) {
            if (text != null) block(text.toString())
        }
    })
}

fun EditText.moveToEnd() {
    setSelection(text.length)
}

inline fun AdapterView<*>.setOnItemSelectedListener(
    crossinline block: (Int) -> Unit
) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
        ) {
            block(position)
        }
    }
}

@MainThread
inline fun <T> LiveData<Event<T>>.observe(
        owner: LifecycleOwner,
        crossinline onChanged: (T) -> Unit
): EventObserver<T> {
    val wrappedObserver = EventObserver<T> { t -> onChanged.invoke(t) }
    observe(owner, wrappedObserver)
    return wrappedObserver
}