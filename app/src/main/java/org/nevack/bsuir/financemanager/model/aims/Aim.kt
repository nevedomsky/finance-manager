package org.nevack.bsuir.financemanager.model.aims

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.nevack.bsuir.financemanager.model.currency.Currency


@Entity(tableName = "aims",
        foreignKeys = [
            ForeignKey(
                    entity = Currency::class,
                    parentColumns = ["code"],
                    childColumns = ["currency"]
            )
        ],
        indices = [Index("currency")])
data class Aim(
        var name: String,
        var target: Double,
        var currency: String,
        var value: Double = 0.0
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    override fun toString(): String = name

    fun getProgress() = (value / target).toFloat()

    fun isCompleted() = target == value

    fun remaining() = target - value
}