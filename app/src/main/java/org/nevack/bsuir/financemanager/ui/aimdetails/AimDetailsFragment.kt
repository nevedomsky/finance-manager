package org.nevack.bsuir.financemanager.ui.aimdetails

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.views.CircularProgressView
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.format

class AimDetailsFragment : Fragment() {

    companion object {
        private const val ARG_NAME = "item"

        fun newInstance(aim: Long): AimDetailsFragment {
            val args = Bundle().apply { putLong(ARG_NAME, aim) }
            return AimDetailsFragment().apply { arguments = args }
        }
    }

    private val viewModel: AimDetailsViewModel by viewModel()

    private lateinit var fabAddPayment: FloatingActionButton
    private lateinit var aimRemainingMessage: TextView

    private lateinit var aimName: TextView
    private lateinit var aimRemainingValue: TextView
    private lateinit var aimTotalValue: TextView
    private lateinit var aimProgress: CircularProgressView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setAim(arguments!!.getLong(ARG_NAME))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater
                .inflate(R.layout.fragment_aim_detail, container, false)

        aimName = view.findViewById(R.id.aim_name)
        aimTotalValue = view.findViewById(R.id.aim_total_value)
        aimRemainingValue = view.findViewById(R.id.aim_remaining_value)
        aimProgress = view.findViewById(R.id.aim_progress)
        aimRemainingMessage = view.findViewById(R.id.aim_remaining_message)

        fabAddPayment = view.findViewById(R.id.fab_add_payment)
        fabAddPayment.setOnClickListener {
            val dialog = AddAimPaymentDialog(viewModel)
            dialog.show(
                    requireActivity().supportFragmentManager,
                    AddAimPaymentDialog.DIALOG_ADD_AIM_PAYMENT
            )
        }

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (activity as? AppCompatActivity)?.setSupportActionBar(toolbar)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.aim.observe(this, Observer {
            aimName.text = it.name
            aimRemainingValue.text = (it.target - it.value).format(0)
            aimTotalValue.text = it.target.format(1)

            if (it.isCompleted()) {
                fabAddPayment.hide()
                aimRemainingMessage.text = getString(R.string.aim_completed_message)
            } else {
                fabAddPayment.show()
                aimRemainingMessage.text = getString(R.string.aim_remaining_message)
            }

            ValueAnimator.ofFloat(0f, it.getProgress()).apply {
                interpolator = DecelerateInterpolator()
                duration = 1_500
                addUpdateListener { anim ->
                    aimProgress.progress = anim.animatedValue as Float
                }
                start()
            }

            aimName.apply {
                alpha = 0f
                translationY = -30f
                scaleX = 0.8f
                scaleY = 0.8f
            }

            aimName.animate()
                    .setDuration(1_000)
                    .alpha(1f)
                    .translationY(0f)
                    .scaleX(1f)
                    .scaleY(1f)
        })
    }
}