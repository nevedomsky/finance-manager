package org.nevack.bsuir.financemanager.model.account

import androidx.annotation.ColorInt
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.nevack.bsuir.financemanager.model.currency.Currency


@Entity(tableName = "accounts",
        foreignKeys = [
            ForeignKey(
                    entity = Currency::class,
                    parentColumns = ["code"],
                    childColumns = ["currency"]
            )
        ],
        indices = [Index("currency")]
)
data class Account(
        var name: String,
        var value: Double = 0.0,
        var currency: String = Currency.BELARUSSIAN.code,
        @ColorInt var color: Int = 0xFF673AB7.toInt()
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    override fun toString(): String = name
}