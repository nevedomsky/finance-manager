package org.nevack.bsuir.financemanager.model.category

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(category: Category) : Long

    @Update
    fun update(vararg categories: Category)

    @Delete
    fun delete(vararg categories: Category)

    @Query("SELECT * FROM categories")
    suspend fun getAllCategories() : List<Category>
    @Query("SELECT * FROM categories")
    fun getAllCategoriesLiveData() : LiveData<List<Category>>

    @Query("SELECT * FROM categories WHERE id=:id")
    fun getCategory(id: Long) : LiveData<Category>

}