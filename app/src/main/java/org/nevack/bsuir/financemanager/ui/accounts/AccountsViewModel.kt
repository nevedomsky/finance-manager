package org.nevack.bsuir.financemanager.ui.accounts

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.account.AccountRepository
import org.nevack.bsuir.financemanager.model.operation.OperationRepository
import kotlin.coroutines.CoroutineContext

class AccountsViewModel(
        private val accountRepository: AccountRepository,
        private val operationRepository: OperationRepository
) : ViewModel(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = IO + job

    private val accountsData: LiveData<List<Account>> = accountRepository.getAccountsLiveData()

    fun getAccounts(): LiveData<List<Account>> = accountsData

    fun createAccount(account: Account) {
        launch {
            accountRepository.createAccount(account)
        }
    }

    val operations = operationRepository.getOperations()

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancelChildren()
    }
}
