package org.nevack.bsuir.financemanager.ui.aims

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.EventObserver
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.ui.aimdetails.AimDetailsActivity

class AimsFragment : Fragment() {

    companion object {
        fun newInstance() = AimsFragment()
    }

    private lateinit var recyclerView: RecyclerView

    private val viewModel: AimsViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_aims, container, false)

        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.aims.observe(this, Observer {
            recyclerView.adapter = AimsAdapter(it, viewModel::openAimDetails)
        })

        viewModel.navigatedToAim.observe(this, EventObserver {
            requireActivity().startActivity(AimDetailsActivity.newIntent(requireContext(), it))
        })
    }
}