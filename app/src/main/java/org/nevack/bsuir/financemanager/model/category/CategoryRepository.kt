package org.nevack.bsuir.financemanager.model.category

class CategoryRepository(
        private val dao: CategoryDao
) {
    suspend fun getCategories() = dao.getAllCategories()
    fun getCategoriesLiveData() = dao.getAllCategoriesLiveData()
}