package org.nevack.bsuir.financemanager

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.get
import org.nevack.bsuir.financemanager.managers.LocaleManager

abstract class BaseActivity : AppCompatActivity() {
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(get<LocaleManager>().updateResources(newBase))
    }
}