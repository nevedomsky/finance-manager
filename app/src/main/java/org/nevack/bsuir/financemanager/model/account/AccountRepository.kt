package org.nevack.bsuir.financemanager.model.account

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class AccountRepository(
        private val accountDao: AccountDao
) : CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    fun createAccount(account: Account) {
        launch { accountDao.insert(account) }
    }

    suspend fun getAccounts() = accountDao.getAllAccounts()

    fun getAccountsLiveData() = accountDao.getAllAccountsLiveData()

    fun getAccount(id: Long) = accountDao.getAccount(id)

    fun removeAccount(account: Account) = accountDao.delete(account)

    fun updateAccount(account: Account) {
        launch { accountDao.update(account) }
    }

    fun updateAccountValue(id: Long, value: Double ) {
        launch {
            accountDao.updateAccountValue(id, value)
        }
    }
}