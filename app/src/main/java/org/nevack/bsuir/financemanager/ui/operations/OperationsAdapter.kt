package org.nevack.bsuir.financemanager.ui.operations

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.format
import org.nevack.bsuir.financemanager.model.operation.Operation
import org.nevack.bsuir.financemanager.model.operation.PopulatedOperation

class OperationsAdapter(
        context: Context,
        private val delete: (Operation) -> Unit,
        private val open: (Operation) -> Unit
) : ListAdapter<PopulatedOperation, OperationsAdapter.OperationViewHolder>(
        OperationDiffCallback()
){
    private val colorIncome = context.getColor(R.color.income_green)
    private val colorSpent = context.getColor(R.color.spent_red)


    class OperationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.operation_name)
        val value: TextView = view.findViewById(R.id.operation_value)
        val category: TextView = view.findViewById(R.id.operation_category)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): OperationViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.operation_item, parent, false)

        return OperationViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: OperationViewHolder, position: Int) {
        val operation = getItem(position)
        holder.name.text = operation.item.name
        holder.value.text = operation.item.value.format()
        holder.category.text = operation.category.name
        holder.itemView.setOnLongClickListener {
            val popup = PopupMenu(it.context, it, Gravity.END or Gravity.CENTER)
            popup.menu.add(R.string.menu_operation_delete).setOnMenuItemClickListener {
                delete(operation.item)
                true
            }
            popup.show()
            true
        }
        holder.itemView.setOnClickListener {
            open(operation.item)
        }
        holder.itemView.tag = operation

        holder.value.setTextColor(getColor(operation.item))
    }

    @ColorInt
    private fun getColor(operation: Operation): Int {
        if (operation.value >= 0) return colorIncome
        return colorSpent
    }
}