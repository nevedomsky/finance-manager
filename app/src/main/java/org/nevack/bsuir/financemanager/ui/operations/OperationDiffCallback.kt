package org.nevack.bsuir.financemanager.ui.operations

import androidx.recyclerview.widget.DiffUtil
import org.nevack.bsuir.financemanager.model.operation.PopulatedOperation

class OperationDiffCallback : DiffUtil.ItemCallback<PopulatedOperation>() {

    override fun areItemsTheSame(
            oldItem: PopulatedOperation,
            newItem: PopulatedOperation
    ): Boolean {
        return oldItem.item.id == newItem.item.id
    }

    override fun areContentsTheSame(
            oldItem: PopulatedOperation,
            newItem: PopulatedOperation
    ): Boolean {
        return oldItem.item == newItem.item
    }
}