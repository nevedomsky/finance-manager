package org.nevack.bsuir.financemanager.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.format

class CircularProgressView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : View(context, attrs, defStyle, defStyleRes) {

    private val completePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColor(R.color.secondaryColor)
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }

    private val backgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = 0x1f000000
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }

    private val textPaint: TextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        color = 0xff000000.toInt()
    }


    private lateinit var rect: RectF
    private val textSize = Rect()

    private var diameter = 0f
    private var strokeWidth = 0f

    var progress: Float = 1f
        set(value) {
            if (value < 0f || value > 1f) return
            field = value
            invalidate()
        }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        val xpad = (paddingLeft + paddingRight).toFloat()
        val ypad = (paddingTop + paddingBottom).toFloat()

        val ww = w.toFloat() - xpad
        val hh = h.toFloat() - ypad
        diameter = Math.min(ww, hh)
        strokeWidth = (Math.log(diameter.toDouble() / 4 + Math.E) * Math.E * Math.PI).toFloat()

        rect = RectF(
                paddingLeft.toFloat() + (ww - diameter) / 2 + strokeWidth,
                paddingTop.toFloat()  + (hh - diameter) / 2 + strokeWidth,
                paddingLeft.toFloat() + (ww + diameter) / 2 - strokeWidth,
                paddingTop.toFloat()  + (hh + diameter) / 2 - strokeWidth
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        backgroundPaint.strokeWidth = strokeWidth
        completePaint.strokeWidth = strokeWidth
        textPaint.textSize = strokeWidth * 2

        val message = (progress * 100).toDouble().format(1) + "%"
        textPaint.getTextBounds(message, 0, message.length, textSize)

        canvas.apply {
            drawArc(rect, 135f, 270f, false, backgroundPaint)
            drawArc(rect, 135f, 270f * progress, false, completePaint)
            drawText(
                    message,
                    (width - textSize.width()) / 2f,
                    (height + diameter) / 2f - strokeWidth,
                    textPaint
            )
        }
    }
}