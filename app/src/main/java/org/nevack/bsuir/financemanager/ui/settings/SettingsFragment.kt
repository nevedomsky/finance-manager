package org.nevack.bsuir.financemanager.ui.settings

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.locale


class SettingsFragment : PreferenceFragmentCompat() {
    private val viewModel: SettingsViewModel by viewModel()

    private val currencyPreference: ListPreference by lazy {
        findPreference("default_currency") as ListPreference
    }

    override fun onCreatePreferences(bundle: Bundle?, key: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getCurrencies().observe(this, Observer {
            currencyPreference.apply {
                val result = it.sortedBy { c -> c.name }
                entries = result.map {
                    currency ->  currency.getName(resources.locale)
                }.toTypedArray()

                entryValues = result.map {
                    currency ->  currency.code
                }.toTypedArray()

                summary = getString(R.string.setting_summary_default_currency)
                icon = context.getDrawable(R.drawable.ic_monetization_black_24dp)
                isEnabled = true
            }
        })

        findPreference<ListPreference>("default_lang").setOnPreferenceChangeListener { preference, newValue ->
            activity?.recreate()
            true
        }
    }
}