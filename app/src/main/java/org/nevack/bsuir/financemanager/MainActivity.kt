package org.nevack.bsuir.financemanager

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.core.view.isVisible
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_main.*
import org.nevack.bsuir.financemanager.ui.accounts.AccountsFragment
import org.nevack.bsuir.financemanager.ui.aims.AimsFragment
import org.nevack.bsuir.financemanager.ui.operations.OperationsFragment
import org.nevack.bsuir.financemanager.ui.settings.SettingsFragment


class MainActivity : BaseActivity() {

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_settings -> {
                supportFragmentManager.commit {
                    replace(R.id.main_container, SettingsFragment())
                    addToBackStack(null)
                }
                navigation.animate()
                        .setDuration(250)
                        .setInterpolator(AccelerateInterpolator())
                        .yBy(navigation.height.toFloat())
                        .withEndAction { navigation.isVisible = false }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) supportFragmentManager.commit {
            add(R.id.main_container, AccountsFragment.newInstance())
        }

        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> {
                    supportFragmentManager.commit {
                        replace(R.id.main_container, AccountsFragment.newInstance())
                    }
                    true
                }
                R.id.navigation_dashboard -> {
                    supportFragmentManager.commit {
                        replace(R.id.main_container, OperationsFragment.newInstance())
                    }
                    true
                }
                else -> {
                    supportFragmentManager.commit {
                        replace(R.id.main_container, AimsFragment.newInstance())
                    }
                    true
                }
            }
        }

        navigation.setOnNavigationItemReselectedListener(null)
    }

    override fun onBackPressed() {
        if (!navigation.isVisible) {
            navigation.animate()
                    .setDuration(200)
                    .setInterpolator(DecelerateInterpolator())
                    .yBy(-navigation.height.toFloat())
                    .withStartAction { navigation.isVisible = true }
        }
        super.onBackPressed()
    }
}
