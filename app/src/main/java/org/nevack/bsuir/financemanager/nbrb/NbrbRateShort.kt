package org.nevack.bsuir.financemanager.nbrb

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class NbrbRateShort(
        @Json(name = "Cur_ID") val curId: Int,
        @Json(name = "Date") val date: Date,
        @Json(name = "Cur_OfficialRate") val officialRate: Double?
)