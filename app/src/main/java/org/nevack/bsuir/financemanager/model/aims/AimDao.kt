package org.nevack.bsuir.financemanager.model.aims

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AimDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(aim: Aim)

    @Update
    fun update(vararg aims: Aim)

    @Delete
    fun delete(vararg aims: Aim)

    @Query("SELECT * FROM aims")
    fun getAllAims() : LiveData<List<Aim>>

    @Query("SELECT * FROM aims WHERE id=:id")
    suspend fun getAim(id: Long) : Aim
}