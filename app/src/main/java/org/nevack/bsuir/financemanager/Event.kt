package org.nevack.bsuir.financemanager

import androidx.lifecycle.Observer


class EventObserver<T>(private val handler: (T) -> Unit)
    : Observer<Event<T>> {
    override fun onChanged(event: Event<T>?) {
        event?.consume()?.let { handler(it) }
    }
}


class Event<T>(private val value: T) {
    private var handled: Boolean = false

    fun peek() : T = value

    fun consume() : T? {
        return if (handled) {
            null
        } else {
            handled = true
            value
        }
    }
}