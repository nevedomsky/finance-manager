package org.nevack.bsuir.financemanager.ui.aimdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import org.nevack.bsuir.financemanager.R

class AddAimPaymentDialog(private val viewModel: AimDetailsViewModel ) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_add_aim_payment, container, false)

        view.findViewById<Button>(R.id.add_payment).setOnClickListener {
            viewModel.addPayment(view.findViewById<EditText>(R.id.aim_payment_value).text.toString().toDouble())
            dismiss()
        }

        viewModel.aim.observe(this, Observer {
            view.findViewById<TextView>(R.id.aim_name).text = it.name
        })

        return view
    }

    companion object {
        const val DIALOG_ADD_AIM_PAYMENT = "dialog_add_aim_payment"
    }
}