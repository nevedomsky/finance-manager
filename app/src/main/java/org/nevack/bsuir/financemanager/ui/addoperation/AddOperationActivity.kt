package org.nevack.bsuir.financemanager.ui.addoperation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import org.nevack.bsuir.financemanager.BaseActivity
import org.nevack.bsuir.financemanager.R

class AddOperationActivity : BaseActivity() {

    companion object {
        const val ARG_NAME = "operation"
        const val OPERATION_NONE = -1L
        fun newIntent(context: Context, id: Long): Intent {
            return Intent(context, AddOperationActivity::class.java).apply {
                putExtra(ARG_NAME, id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                val fragment = if (intent.hasExtra(ARG_NAME)) {
                    AddOperationFragment.newInstance(
                            intent.getLongExtra(ARG_NAME, OPERATION_NONE))
                } else {
                    AddOperationFragment.newInstance()
                }

                add(R.id.container, fragment)
            }
        }
    }
}