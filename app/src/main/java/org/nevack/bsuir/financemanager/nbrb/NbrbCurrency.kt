package org.nevack.bsuir.financemanager.nbrb

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class NbrbCurrency(
        @Json(name = "Cur_ID") val id: Int,
        @Json(name = "Cur_ParentID") val parentId: Int?,
        @Json(name = "Cur_Code") val code: String,
        @Json(name = "Cur_Abbreviation") val abbreviation: String,

        @Json(name = "Cur_Name") val nameRus: String,
        @Json(name = "Cur_Name_Bel") val nameBel: String,
        @Json(name = "Cur_Name_Eng") val name: String,

        @Json(name = "Cur_QuotName") val quotNameRus: String,
        @Json(name = "Cur_QuotName_Bel") val quotNameBel: String,
        @Json(name = "Cur_QuotName_Eng") val quotName: String,

        @Json(name = "Cur_NameMulti") val nameMultiRus: String,
        @Json(name = "Cur_Name_BelMulti") val nameMultiBel: String,
        @Json(name = "Cur_Name_EngMulti") val nameMulti: String,

        @Json(name = "Cur_Scale") val scale: Int,
        @Json(name = "Cur_Periodicity") val periodicity: Int,
        @Json(name = "Cur_DateStart") val dateStart: Date,
        @Json(name = "Cur_DateEnd") val dateEnd: Date
)