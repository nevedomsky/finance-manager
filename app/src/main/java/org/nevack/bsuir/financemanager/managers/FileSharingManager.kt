package org.nevack.bsuir.financemanager.managers

import android.content.Context
import android.content.Intent
import android.content.Intent.*
import androidx.core.content.FileProvider
import org.nevack.bsuir.financemanager.BuildConfig
import org.nevack.bsuir.financemanager.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class FileSharingManager (private val context: Context) {

    companion object {
        private const val AUTHORITY = BuildConfig.APPLICATION_ID + ".export.fileprovider"
        private val REPORT_FORMATTER = SimpleDateFormat("'Report_'dd-MMM-yyyy_HH-mm-ss'.txt'", Locale.US)
    }

    fun reportSharingIntent(text: String): Intent {
        val exportsDir = File(context.filesDir, "exports")
        if (!exportsDir.exists()) exportsDir.mkdir()
        val name = REPORT_FORMATTER.format(Date())
        val file = File(exportsDir, name)
        file.createNewFile()
        file.writeText(text)

        return sharingIntent(file)
    }

    fun sharingIntent(file: File, fileType: String = "text/*"): Intent {

        val uri = FileProvider.getUriForFile(context, AUTHORITY, file)

        return Intent().apply {
            action = ACTION_SEND
            putExtra(EXTRA_TEXT, context.getString(R.string.message_external_report_shared))
            putExtra(EXTRA_STREAM, uri)
            type = fileType
            addFlags(FLAG_GRANT_READ_URI_PERMISSION)
        }
    }
}