package org.nevack.bsuir.financemanager.ui.aims

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.nevack.bsuir.financemanager.Event
import org.nevack.bsuir.financemanager.model.aims.Aim
import org.nevack.bsuir.financemanager.model.aims.AimsRepository

class AimsViewModel(
        repository: AimsRepository
) : ViewModel() {
    val aims: LiveData<List<Aim>> = repository.getAims()

    private val _navigateAimEvent = MutableLiveData<Event<Long>>()
    val navigatedToAim: LiveData<Event<Long>>
        get() = _navigateAimEvent

    fun openAimDetails(id: Long) {
        _navigateAimEvent.value = Event(id)
    }
}