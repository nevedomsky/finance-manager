package org.nevack.bsuir.financemanager.model.operation

import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.category.Category

data class PopulatedOperation(
        val item: Operation,
        val account: Account,
        val category: Category
)