package org.nevack.bsuir.financemanager.model

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.account.AccountDao
import org.nevack.bsuir.financemanager.model.aims.Aim
import org.nevack.bsuir.financemanager.model.aims.AimDao
import org.nevack.bsuir.financemanager.model.category.Category
import org.nevack.bsuir.financemanager.model.category.CategoryDao
import org.nevack.bsuir.financemanager.model.currency.Currency
import org.nevack.bsuir.financemanager.model.currency.CurrencyDao
import org.nevack.bsuir.financemanager.model.operation.HistoryDao
import org.nevack.bsuir.financemanager.model.operation.Operation


const val DATABASE_NAME = "money-db"

@Database(
        entities = [
            Account::class,
            Operation::class,
            Category::class,
            Currency::class,
            Aim::class
        ],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class MoneyDatabase : RoomDatabase() {

    abstract fun accountsDao(): AccountDao
    abstract fun currenciesDao(): CurrencyDao
    abstract fun historyDao(): HistoryDao
    abstract fun categoryDao(): CategoryDao
    abstract fun aimDao(): AimDao
}