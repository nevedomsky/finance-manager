package org.nevack.bsuir.financemanager.model.account

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AccountDao {

    @Insert
    fun insert(account: Account) : Long

    @Update
    fun update(account: Account)

    @Delete
    fun delete(account: Account)

    @Query("SELECT * FROM accounts")
    suspend fun getAllAccounts() : List<Account>

    @Query("SELECT * FROM accounts")
    fun getAllAccountsLiveData() : LiveData<List<Account>>

    @Query("SELECT * FROM accounts WHERE id=:id")
    fun getAccount(id: Long) : LiveData<Account>

    @Query("SELECT * FROM accounts WHERE id=:id")
    suspend fun getAccountSync(id: Long) : Account

    @Transaction
    @Query("SELECT * FROM accounts WHERE id=:id")
    fun getAccountWithHistory(id: Long) : LiveData<AccountWithHistory>

    @Query("UPDATE accounts SET value=:value WHERE id=:id")
    fun updateAccountValue(id: Long, value: Double)
}