package org.nevack.bsuir.financemanager.model

import androidx.room.TypeConverter
import java.util.*

class Converters {
    @TypeConverter
    fun calendarToDatestamp(date: Date): Long = date.time

    @TypeConverter
    fun datestampToCalendar(value: Long): Date = Date(value)
}