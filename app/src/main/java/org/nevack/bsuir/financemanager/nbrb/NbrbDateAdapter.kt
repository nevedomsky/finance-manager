package org.nevack.bsuir.financemanager.nbrb

import android.icu.text.SimpleDateFormat
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.util.*

class NbrbDateAdapter : JsonAdapter<Date>() {

    private val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

    override fun fromJson(reader: JsonReader): Date = format.parse(reader.nextString())

    override fun toJson(writer: JsonWriter, value: Date?) {
        writer.value(format.format(value))
    }
}