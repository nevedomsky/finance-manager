package org.nevack.bsuir.financemanager.ui.aims

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.nevack.bsuir.financemanager.views.HorizontalProgressView
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.model.aims.Aim

class AimsAdapter(
        private val aims: List<Aim>,
        private val open: (Long) -> Unit
) : RecyclerView.Adapter<AimsAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_aim, parent, false))
    }

    override fun getItemCount(): Int = aims.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        with(aims[position]) {
            holder.name.text = name
            holder.progress.progress = (value / target).toFloat()
            holder.remaining.text = holder.itemView.context.getString(R.string.aim_remaining_text, target - value)
        }

        holder.itemView.setOnClickListener {
            open(aims[holder.adapterPosition].id)
        }
    }


    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.aim_name)
        val remaining: TextView = view.findViewById(R.id.aim_remaining)
        val progress: HorizontalProgressView = view.findViewById(R.id.aim_progress)
    }
}