package org.nevack.bsuir.financemanager

import androidx.preference.PreferenceManager
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.module
import org.nevack.bsuir.financemanager.managers.NetworkManager
import org.nevack.bsuir.financemanager.managers.FileSharingManager
import org.nevack.bsuir.financemanager.managers.LocaleManager
import org.nevack.bsuir.financemanager.model.DATABASE_NAME
import org.nevack.bsuir.financemanager.model.MoneyDatabase
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.account.AccountRepository
import org.nevack.bsuir.financemanager.model.aims.Aim
import org.nevack.bsuir.financemanager.model.aims.AimsRepository
import org.nevack.bsuir.financemanager.model.category.Category
import org.nevack.bsuir.financemanager.model.category.CategoryRepository
import org.nevack.bsuir.financemanager.model.currency.Currency
import org.nevack.bsuir.financemanager.model.currency.CurrencyRepository
import org.nevack.bsuir.financemanager.model.operation.OperationRepository
import org.nevack.bsuir.financemanager.nbrb.NbrbDateAdapter
import org.nevack.bsuir.financemanager.nbrb.NbrbService
import org.nevack.bsuir.financemanager.ui.accounts.AccountsViewModel
import org.nevack.bsuir.financemanager.ui.addoperation.AddOperationViewModel
import org.nevack.bsuir.financemanager.ui.aimdetails.AimDetailsViewModel
import org.nevack.bsuir.financemanager.ui.aims.AimsViewModel
import org.nevack.bsuir.financemanager.ui.operations.OperationsViewModel
import org.nevack.bsuir.financemanager.ui.settings.SettingsViewModel
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.util.*


val appModule = module {

    single {
        Moshi.Builder()
                .add(Date::class.java, NbrbDateAdapter())
                .build()
    }
    single {
        Retrofit.Builder()
                .client(OkHttpClient.Builder()
                        .addInterceptor(ChuckInterceptor(androidApplication()))
                        .build())
                .baseUrl("https://www.nbrb.by/API/ExRates/")
                .addConverterFactory(MoshiConverterFactory.create(get()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
    }

    single { get<Retrofit>().create<NbrbService>() }

    single {
        Room.databaseBuilder(
                androidApplication(),
                MoneyDatabase::class.java, DATABASE_NAME
        ).addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                GlobalScope.launch(Dispatchers.IO) {
                    get<MoneyDatabase>().run {
                        currenciesDao().insert(Currency.BELARUSSIAN)
                        accountsDao().insert(Account("Default", 1000.0))
                        categoryDao().insert(Category.DEFAULT)
                        aimDao().insert(
                                Aim("iPhone XS Max", 2500.0, Currency.BELARUSSIAN.code, 1000.0)
                        )
                    }
                }
            }
        }).build()
    }

    single { CurrencyRepository(get<MoneyDatabase>().currenciesDao(), get(), get(), get()) }
    single { AccountRepository(get<MoneyDatabase>().accountsDao()) }
    single { CategoryRepository(get<MoneyDatabase>().categoryDao()) }
    single { OperationRepository(get<MoneyDatabase>().historyDao(), get<MoneyDatabase>().accountsDao()) }
    single { AimsRepository(get<MoneyDatabase>().aimDao()) }

    single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
    single { NetworkManager(androidContext()) }
    single { FileSharingManager(androidContext()) }
    single { LocaleManager(get()) }

    viewModel<SettingsViewModel>()
    viewModel<AccountsViewModel>()
    viewModel<AddOperationViewModel>()
    viewModel<OperationsViewModel>()
    viewModel<AimsViewModel>()
    viewModel<AimDetailsViewModel>()
}