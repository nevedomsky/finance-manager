package org.nevack.bsuir.financemanager.ui.addoperation

import androidx.lifecycle.*
import org.nevack.bsuir.financemanager.Event
import org.nevack.bsuir.financemanager.model.account.AccountRepository
import org.nevack.bsuir.financemanager.model.category.CategoryRepository
import org.nevack.bsuir.financemanager.model.currency.CurrencyRepository
import org.nevack.bsuir.financemanager.model.operation.Operation
import org.nevack.bsuir.financemanager.model.operation.OperationRepository

class AddOperationViewModel(
        private val operationRepository: OperationRepository,
        private val categoryRepository: CategoryRepository,
        private val accountRepository: AccountRepository,
        private val currencyRepository: CurrencyRepository
) : ViewModel() {

    private val _operation = MutableLiveData<Long>()
    val operation: LiveData<Operation> = Transformations.switchMap(_operation) {
        if (it < 0) {
            return@switchMap MutableLiveData(Operation("", -1, -1, .0))
        }
        operationRepository.getOperation(it)
    }

    fun setOperation(id: Long) {
        _operation.value = id
    }

    private val _valueError = MutableLiveData<Event<Boolean>>()
    val valueError: LiveData<Event<Boolean>>
        get() = _valueError

    private val _nameError = MutableLiveData<Event<Boolean>>()
    val nameError: LiveData<Event<Boolean>>
        get() = _nameError


    private val _error = MutableLiveData<Event<String>>()
    val error: LiveData<Event<String>>
        get() = _error

    private val _saved = MutableLiveData<Event<Boolean>>()
    val saved: LiveData<Event<Boolean>>
        get() = _saved

    fun addOperationToAccount() {
        val account = accounts.value?.get(selectedAccount)?.id

        if (account == null) {
            _error.value = Event("Invalid account")
            return
        }

        val category = categories.value?.get(selectedCategory)?.id

        if (category == null) {
            _error.value = Event("Invalid category")
            return
        }

        val type = if (isSpent) -1 else 1

        val operation = operation.value!!
        operation.apply {
            name = _name
            value = type * _value
            this.category = category
            this.account = account
        }

        if (_operation.value ?: -1 < 0 ) {
            operationRepository.addOperation(operation)
        } else {
            operationRepository.updateOperation(operation)
        }

        _saved.value = Event(true)
    }

    val categories = categoryRepository.getCategoriesLiveData()
    val accounts = accountRepository.getAccountsLiveData()
    val currencies = currencyRepository.getCurrencies()

    var selectedAccount: Int= 0
    var selectedCategory: Int = 0
    var isSpent: Boolean = true

    private var _name: String = ""
    var name: String
        set(value) {
            _nameError.value = Event(value.isBlank())
            _name = value
        }
        get() = _name

    private var _value = 0.0
    var value: String
        set(value) {
            if (value.isBlank() || value == "0") {
                _valueError.value = Event(true)
                return
            }
            val double = value.toDoubleOrNull()
            if (double == null) {
                _valueError.value = Event(true)
                return
            }

            _valueError.value = Event(double == 0.0)

            _value = double
        }
        get() = _value.toString()
}
