package org.nevack.bsuir.financemanager.nbrb

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path


interface NbrbService {
    @GET("Currencies/")
    fun currencies(): Deferred<List<NbrbCurrency>>

    @GET("Rates?Periodicity=0")
    fun rates(): Deferred<List<NbrbRate>>

    @GET("Rates/{name}?Periodicity=0&ParamMode=2")
    fun rates(@Path("name") name: String): Deferred<List<NbrbRate>>
}