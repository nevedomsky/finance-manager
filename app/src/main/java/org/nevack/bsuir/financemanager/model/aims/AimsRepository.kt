package org.nevack.bsuir.financemanager.model.aims

import androidx.lifecycle.LiveData

class AimsRepository(
        private val dao: AimDao
) {
    fun getAims(): LiveData<List<Aim>> = dao.getAllAims()

    suspend fun getAim(id: Long) = dao.getAim(id)

    fun addAim(aim: Aim) = dao.insert(aim)

    fun updateAim(aim: Aim) = dao.update(aim)
}