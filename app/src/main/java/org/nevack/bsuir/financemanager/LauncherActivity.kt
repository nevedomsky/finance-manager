package org.nevack.bsuir.financemanager

import android.os.Bundle
import androidx.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit

class LauncherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(PreferenceManager.getDefaultSharedPreferences(applicationContext)) {
            if (!getBoolean("first_launch", false)) {
                edit {
                    putString("default_currency", "BYN")
                    putBoolean("first_launch", true)
                }
            }
        }

        startActivity<MainActivity>()
        finish()
    }
}