package org.nevack.bsuir.financemanager.ui.operations

import androidx.lifecycle.*
import kotlinx.coroutines.*
import org.nevack.bsuir.financemanager.Event
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.account.AccountRepository
import org.nevack.bsuir.financemanager.model.category.Category
import org.nevack.bsuir.financemanager.model.category.CategoryRepository
import org.nevack.bsuir.financemanager.model.operation.Operation
import org.nevack.bsuir.financemanager.model.operation.OperationRepository
import org.nevack.bsuir.financemanager.model.operation.PopulatedOperation

class OperationsViewModel(
        private val operationRepository: OperationRepository,
        private val accountRepository: AccountRepository,
        private val categoryRepository: CategoryRepository
) : ViewModel() {

    private val _accounts = MutableLiveData<Map<Long, Account>>()
    private val _categories = MutableLiveData<Map<Long, Category>>()

    private val parentJob = Job()
    private val coroutineScope = CoroutineScope(parentJob)

    private val _navigateToDetails = MutableLiveData<Event<Long>>()
    val navigateToDetails: LiveData<Event<Long>>
        get() = _navigateToDetails

    fun openOperationDetails(id: Long) {
        _navigateToDetails.value = Event(id)
    }

    private var _operations: LiveData<List<Operation>>
    val operations: LiveData<List<PopulatedOperation>>

    val addedEvent: LiveData<Event<Unit>> = operationRepository.operationAdded
    val removedEvent: LiveData<Event<Unit>> = operationRepository.operationRemoved

    init {
        coroutineScope.launch(Dispatchers.IO) {
            val accounts = accountRepository.getAccounts().associateBy { it.id }
            val categories = categoryRepository.getCategories().associateBy { it.id }

            withContext(Dispatchers.Main) {
                _accounts.value = accounts
                _categories.value = categories
            }
        }

        _operations = Transformations.switchMap(_accounts) {
            Transformations.switchMap(_categories) {
                operationRepository.getOperations()
            }
        }

        operations = Transformations.map(_operations) {
            it.map { o ->  PopulatedOperation(o, _accounts.value!![o.account]!!, _categories.value!![o.category]!! ) }
        }
    }

    fun removeOperation(operation: Operation) {
        operationRepository.removeOperation(operation)
    }
}
