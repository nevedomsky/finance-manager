package org.nevack.bsuir.financemanager.managers

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import java.util.*

class LocaleManager(
        private val prefs: SharedPreferences
) {
    fun updateResources(context: Context) : Context {
        val language = prefs.getString("default_lang", "en")

        val locale = Locale(language)
        Locale.setDefault(locale)

        val res = context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }
}