package org.nevack.bsuir.financemanager.model.currency

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.nevack.bsuir.financemanager.nbrb.NbrbCurrency
import org.nevack.bsuir.financemanager.nbrb.NbrbRate
import java.util.*
import kotlin.collections.HashMap

@Entity(tableName = "currencies")
data class Currency(
        @PrimaryKey var code: String,
        var name: String,
        var scale: Int,
        var rate: Double
) {
    @Ignore private val names: HashMap<String, String>

    init {
        try {
            names = HashMap()
            name.split(';').forEach {
                val entry = it.split(',')
                names[entry[0]] = entry[1]
            }
        } catch (ex: IndexOutOfBoundsException) {
            throw IllegalArgumentException("String should be in format - \"code,value;...\"", ex)
        }
    }

    fun getName(locale: Locale): String {
        return names[locale.language] ?: names["en"] ?: throw Exception("Could not find locale")
    }

    companion object {
        val BELARUSSIAN = Currency(
                "BYN", "en,Belarussian ruble;ru,Белорусский рубль;be,Беларускi рубель", 1, 1.0)

        fun fromNbrb(nbrbCurrency: NbrbCurrency, nbrbRate: NbrbRate): Currency {
            val s = "en,${nbrbCurrency.name};ru,${nbrbCurrency.nameRus};be,${nbrbCurrency.nameBel}"
            return Currency(nbrbRate.abbreviation, s, nbrbRate.scale, nbrbRate.officialRate
                    ?: .0)
        }
    }
}