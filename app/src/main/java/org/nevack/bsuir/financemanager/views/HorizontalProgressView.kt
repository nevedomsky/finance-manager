package org.nevack.bsuir.financemanager.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import org.nevack.bsuir.financemanager.R
import org.nevack.bsuir.financemanager.lerp

/**
 * TODO: document your custom view class.
 */
class HorizontalProgressView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : View(context, attrs, defStyle, defStyleRes) {

    private val completePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColor(R.color.secondaryColor)
    }

    private val backgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = 0x1f000000
    }


    private lateinit var rect: RectF

    var progress: Float = 0.0f
        set(value) {
            field = value
            invalidate()
        }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        val xpad = (paddingLeft + paddingRight).toFloat()
        val ypad = (paddingTop + paddingBottom).toFloat()

        val ww = width.toFloat() - xpad
        val hh = height.toFloat() - ypad

        rect = RectF(
                paddingLeft.toFloat(),
                paddingTop.toFloat(),
                paddingLeft.toFloat() + (hh to ww lerp progress),
                (h - paddingBottom).toFloat()
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val xpad = (paddingLeft + paddingRight).toFloat()
        val ypad = (paddingTop + paddingBottom).toFloat()

        val ww = width.toFloat() - xpad
        val hh = height.toFloat() - ypad

        canvas.apply {
//            val h = height.toFloat()
//            val w = width.toFloat()
            drawRoundRect(paddingLeft.toFloat(), paddingTop.toFloat(), (width - paddingRight).toFloat(), (height - paddingBottom).toFloat(), hh / 2, hh / 2, backgroundPaint)
            drawRoundRect(rect.apply { right = paddingLeft.toFloat() + (hh to ww).lerp(progress) }, hh / 2, hh / 2, completePaint)
        }
    }
}
