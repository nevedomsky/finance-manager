package org.nevack.bsuir.financemanager.ui.aimdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.nevack.bsuir.financemanager.model.aims.Aim
import org.nevack.bsuir.financemanager.model.aims.AimsRepository
import kotlin.coroutines.CoroutineContext
import kotlin.math.min

class AimDetailsViewModel(
        private val aimsRepository: AimsRepository
) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private val job = Job()

    private val _aim = MutableLiveData<Aim>()
    val aim: LiveData<Aim>
        get() = _aim

    fun setAim(id: Long) {
        launch {
            val aim = aimsRepository.getAim(id)
            withContext(Dispatchers.Main) {
                _aim.value = aim
            }
        }
    }

    fun addPayment(payment: Double) {
        val aim = _aim.value
        if (aim != null) {
            aim.value += min(aim.remaining(), payment)

            launch {
                aimsRepository.updateAim(aim)
            }
            _aim.value = aim
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}