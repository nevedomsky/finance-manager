package org.nevack.bsuir.financemanager.ui.aimdetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import org.nevack.bsuir.financemanager.BaseActivity
import org.nevack.bsuir.financemanager.R

class AimDetailsActivity : BaseActivity() {
    companion object {
        private const val ARG = "aim"
        fun newIntent(context: Context, id: Long): Intent {
            return Intent(context, AimDetailsActivity::class.java).apply {
                putExtra(ARG, id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.container, AimDetailsFragment.newInstance(intent.getLongExtra(ARG, -1)))
            }
        }
    }
}