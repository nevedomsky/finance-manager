package org.nevack.bsuir.financemanager

import android.app.Application
import android.content.Context
import org.koin.android.ext.android.get
import org.koin.android.ext.android.startKoin
import org.nevack.bsuir.financemanager.managers.LocaleManager


@Suppress("unused")
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }

//    override fun attachBaseContext(base: Context) {
//        super.attachBaseContext(get<LocaleManager>().updateResources(base))
//    }
}