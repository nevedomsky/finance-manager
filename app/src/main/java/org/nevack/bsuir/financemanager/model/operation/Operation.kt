package org.nevack.bsuir.financemanager.model.operation

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.nevack.bsuir.financemanager.model.account.Account
import org.nevack.bsuir.financemanager.model.category.Category
import java.util.*


@Entity(tableName = "history",
        foreignKeys = [
            ForeignKey(
                    entity = Category::class,
                    parentColumns = ["id"],
                    childColumns = ["category"]
            ),
            ForeignKey(
                    entity = Account::class,
                    parentColumns = ["id"],
                    childColumns = ["account"],
                    onDelete = ForeignKey.CASCADE
            )
        ],
        indices = [Index("account"), Index("category")])
data class Operation (
        var name: String,
        var category: Long,
        var account: Long,
        var value: Double = 0.0,
        var date: Date = Date()
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    override fun toString(): String = name
}