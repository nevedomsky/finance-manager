package org.nevack.bsuir.financemanager.model.account

import androidx.room.Embedded
import androidx.room.Relation
import org.nevack.bsuir.financemanager.model.operation.Operation

class AccountWithHistory {
    @Embedded
    var account: Account? = null

    @Relation(parentColumn = "id", entityColumn = "account")
    var history: List<Operation> = arrayListOf()
}