package org.nevack.bsuir.financemanager.model.category

import androidx.annotation.ColorInt
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "categories")
data class Category (
        var name: String,
        @ColorInt var color: Int = 0x3F3F3F /* 75% black */
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    override fun toString(): String = name

    companion object {
        val DEFAULT = Category("Default", 0xFF7F7F7F.toInt())
    }
}