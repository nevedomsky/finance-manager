package org.nevack.bsuir.financemanager.ui.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.nevack.bsuir.financemanager.model.currency.CurrencyRepository

class SettingsViewModelFactory(
        private val repository: CurrencyRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = SettingsViewModel(repository) as T
}