package org.nevack.bsuir.financemanager.views

import android.content.Context
import android.util.AttributeSet
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.Spinner
import com.google.android.material.textfield.TextInputLayout
import org.nevack.bsuir.financemanager.R


class MySpinner @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    private val spinner: Spinner
    private val textLayout: TextInputLayout
    private val editText: TextInputLayout

    init {
        inflate(context, R.layout.custom_spinner, this)

        spinner = findViewById(R.id.myspinner_spinner)
        textLayout = findViewById(R.id.myspinner_textinputlayout)
        editText = findViewById(R.id.myspinner_textinputedittext)
    }

    fun setData(data: List<Any>) {
        spinner.adapter = ArrayAdapter(
                context,
                android.R.layout.simple_list_item_1,
                data
        )
    }

    fun setSelectedListener(listener: AdapterView.OnItemSelectedListener) {
        spinner.onItemSelectedListener = listener
    }

    fun setHint(hint: String) = textLayout::setHint
}