package org.nevack.bsuir.financemanager.model.operation

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface HistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(operation: Operation)

    @Update
    fun update(vararg operations: Operation)

    @Delete
    fun delete(vararg operations: Operation)

    @Query("SELECT * FROM history ORDER BY date DESC")
    fun getAllHistory() : LiveData<List<Operation>>

    @Query("SELECT * FROM history WHERE account=:account")
    fun getHistoryFromAccount(account: Long) : LiveData<List<Operation>>

    @Query("SELECT * FROM history WHERE id=:id")
    fun getOperationById(id: Long) : LiveData<Operation>


    @Query("SELECT * FROM history WHERE id=:id")
    suspend fun getOperationSync(id: Long) : Operation

}