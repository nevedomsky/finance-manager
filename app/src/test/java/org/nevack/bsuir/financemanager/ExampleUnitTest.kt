package org.nevack.bsuir.financemanager

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun lerp_isCorrect() {
        assertEquals(lerp(2.0f, 4.0f, 0.5f), 3.0f)
    }

    @Test
    fun lerpPair_isCorrect() {
        assertEquals((2.0f to 4.0f).lerp(0.75f), 3.5f)
    }
}
